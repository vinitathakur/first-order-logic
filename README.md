# First Order Logic

### Introduction
* Creates a Knowledge Base using the information provided in the form of facts
* Queries can then be answered from the built Knowledge Base using First Order Logic

### I/O Files
* '''input.txt''' file contains facts as well as queries to be run on those facts. The first line is the number of queries followed by those many queries. The next line contains the number of facts followed by those many facts
* '''output.txt''' files contains a bunch of booleans indicating the results of the queries in the input file

### How to run?
* The file '''FirstOrderLogic.java''' contains the main method and hence needs to be run