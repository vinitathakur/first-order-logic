import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;


class FirstOrderLogic {
	
	public static WrappedInput readInput() throws IOException
	{
		BufferedReader br = new BufferedReader(new FileReader("input.txt"));
		int query_count = Integer.parseInt(br.readLine());
		ArrayList<ArrayList<Predicate>> kb = new ArrayList<>();
		Predicate queries[] = new Predicate[query_count];
		int i = 0;
		while(query_count>0)
		{
			String query = br.readLine();
			Predicate obj = convertToObj(query);
			queries[i] = obj;
			query_count--;
			i++;
		}
		
		int kb_count = Integer.parseInt(br.readLine());
		
		while(kb_count>0)
		{
			ArrayList<Predicate> one_sentence = new ArrayList<>();
			String sentence = br.readLine();
			String kb_arr[] = sentence.split("\\|");
			for (String predicate : kb_arr)
			{
				one_sentence.add(convertToObj(predicate.trim()));
			}
			kb.add(one_sentence);
			kb_count--;
		}
		
		
		br.close();
		
		WrappedInput input_obj = new WrappedInput();
		input_obj.queries = queries;
		input_obj.ar = kb;
		
		return input_obj;
	}
	
	public static HashMap<String,ArrayList<ArrayList<Predicate>>> createKB(ArrayList<ArrayList<Predicate>> lol)
	{
		HashMap<String,ArrayList<ArrayList<Predicate>>> knowledge_base = new HashMap<>();
		for(int i=0; i<lol.size();i++)
		{
			for (int j=0; j<lol.get(i).size(); j++)
			{
				String key = "";
				if (lol.get(i).get(j).is_negated)
					key = "~"+lol.get(i).get(j).name;
				else
					key = lol.get(i).get(j).name;
				
				ArrayList<ArrayList<Predicate>> temp;
				
				ArrayList<Predicate> internal = new ArrayList<>(lol.get(i));
				
				Predicate removed_elem = internal.get(j);					
				internal.remove(j);
				internal.add(0, removed_elem);
				
				if(!knowledge_base.containsKey(key))
				{
					
					temp = new ArrayList<>();
					temp.add(internal);
					knowledge_base.put(key, temp);
					
				}
				else
				{	
					
					temp = knowledge_base.get(key);
					temp.add(internal);
					knowledge_base.put(key, temp);
					
				}
				
			}
		}
		return knowledge_base;
	}
	
	
	/*This method updates the KB passed by reference
	 * Only a single predicate can be inserted
	 */
	public static boolean addToKB(ArrayList<Predicate> predicates, HashMap<String,ArrayList<ArrayList<Predicate>>> knowledge_base)
	{
		boolean return_flag = false;
		
		for (int i=0; i<predicates.size(); i++)
		{
			Predicate single_obj = predicates.get(i);
			String key = "";
			if (single_obj.is_negated)
				key = "~"+single_obj.name;
			else
				key = single_obj.name;
			
			
			ArrayList<Predicate> reordered = new ArrayList<>(predicates);
			Predicate removed_elem = reordered.get(i);
			reordered.remove(i);
			reordered.add(0, removed_elem);
			
			
			if (!knowledge_base.containsKey(key))
			{
				ArrayList<ArrayList<Predicate>> temp = new ArrayList<>();
				temp.add(reordered);	
				knowledge_base.put(key, temp);
			}
			else
			{
				
				ArrayList<ArrayList<Predicate>> received = knowledge_base.get(key);
				
				ArrayList<String> reordered_str = new ArrayList<>();
				for (Predicate pd : reordered)
				{
					reordered_str.add(predicateStringRep(pd));
				}
				
				ArrayList<String> dedup = new ArrayList<>(checkDuplicatesWithin(reordered_str));
				Collections.sort(dedup);
				
				ArrayList<ArrayList<String>> received_str = new ArrayList<>();
				for (ArrayList<Predicate> inner : received)
				{
					ArrayList<String> received_inner_list = new ArrayList<>(); 
					for (Predicate inner_obj : inner)
					{
						received_inner_list.add(predicateStringRep(inner_obj));
					}
					Collections.sort(received_inner_list);
					received_str.add(received_inner_list);
				}
				
				
				boolean inner_flag = false;
				
				for (ArrayList<String> list_level : received_str)
				{
					
					if (list_level.size() == dedup.size())
					{
						inner_flag = true;
						for (int xy=0; xy<dedup.size(); xy++)
						{
							if (!dedup.get(xy).equals(list_level.get(xy)))
							{
								inner_flag = false;
								break;
							}
						}
						if (inner_flag)
							break;
						
					}
				}
				
				
				if (!inner_flag)
				{
					ArrayList<Predicate> based_on_dedup = new ArrayList<>();
					for (String x:dedup)
					{
						based_on_dedup.add(convertToObj(x));
					}
					received.add(based_on_dedup);
					knowledge_base.put(key, received);
					
					return_flag = true;
				}
				
			}
		}
		return return_flag;
	}
	
	public static Predicate convertToObj(String pred)
	{
		Predicate obj = new Predicate();
		int start_at = 0;
		if (pred.charAt(0)=='~')
		{
			obj.is_negated = true;
			start_at = 1;
		}
		obj.name = pred.substring(start_at, pred.indexOf('('));
		obj.variables = pred.substring(pred.indexOf('(')+1, pred.indexOf(')')).split(",");
		for (int s=0; s<obj.variables.length;s++)
		{
			obj.variables[s] = obj.variables[s].trim();
		}
		
		return obj;
	}
	
	public static String predicateStringRep(Predicate obj)
	{
		StringBuffer string_form = new StringBuffer();
		if (obj.is_negated)
		{
			string_form.append("~");
		}
		string_form.append(obj.name);
		string_form.append("(");
		for(int i=0; i< obj.variables.length; i++)
		{
			string_form.append(obj.variables[i]+",");
		}
		string_form.deleteCharAt(string_form.length()-1);
		string_form.append(")");
		
		return string_form.toString();
	}
	
	public static void printKB(HashMap<String,ArrayList<ArrayList<Predicate>>> hm)
	{
		for (String key : hm.keySet())
		{
			System.out.print(key+" ");
			ArrayList<ArrayList<Predicate>> value = hm.get(key);
			for (int i=0; i< value.size(); i++)
			{
				System.out.print("[ ");
				for (int j=0; j<value.get(i).size(); j++)
				{
					System.out.print(predicateStringRep(value.get(i).get(j))+" ");
				}
				System.out.print(" ] ");
			}
			System.out.println();
			
		}
	}
	
	public static boolean canQueryBeInferred(Predicate query, HashMap<String,ArrayList<ArrayList<Predicate>>> kb)
	{
		
		HashMap<String, ArrayList<ArrayList<Predicate>>> knowledge_base = cloneHashMap(kb);
		
		//Step 1 :Negate the query and add it to the KB
		if (!query.is_negated)
			query.is_negated = true;
		else
			query.is_negated = false;
		
		ArrayList<Predicate> intermediate = new ArrayList<>();
		intermediate.add(query);
		addToKB(intermediate,knowledge_base);
		
		boolean changes_made = true;
		
		int iter = 0;
		long start_time2 = System.currentTimeMillis()/1000;
		while(changes_made && System.currentTimeMillis()/1000 - start_time2 < 120)
		{
			System.out.println("------------------------------"+iter);
			iter++;
			ArrayList<ArrayList<Predicate>> temp_list = new ArrayList<>();
			int count = 0;
			for(String key : knowledge_base.keySet())
			{	
				String search_for = "";
				if (key.charAt(0)=='~')
					search_for = key.substring(1,key.length());
				else
					search_for = "~"+key;
				
				if (knowledge_base.containsKey(search_for))
				{
					ArrayList<ArrayList<Predicate>> first = knowledge_base.get(key);
					ArrayList<ArrayList<Predicate>> second = knowledge_base.get(search_for);
					
					for (int i=0; i<first.size() && System.currentTimeMillis()/1000 - start_time2 < 120; i++)
					{
						for (int j=0; j<second.size() && System.currentTimeMillis()/1000 - start_time2 < 120; j++)
						{
							ArrayList<ArrayList<Predicate>> resolution_answer = resolve(first.get(i), second.get(j));
							for (ArrayList<Predicate> single_arr: resolution_answer)
							{
								if (single_arr.size()==0)
									return true;
								temp_list.add(single_arr);
							}
						} 
					}
				}	
			}
			
			for (ArrayList<Predicate> single : temp_list)
			{
				if (System.currentTimeMillis()/1000 - start_time2 > 20)
					return false;
				boolean got_added = addToKB(single, knowledge_base);
				if (got_added)
				{
					count++;
				}
			}
			
			System.out.println(count);
			
			//while loop breaking condition
			if (count == 0)
			{
				changes_made = false;
			}
				
		}
		return false;
	}
	
	
	public static ArrayList<ArrayList<Predicate>> resolve(ArrayList<Predicate> first, ArrayList<Predicate> second)
	{
		ArrayList<ArrayList<Predicate>> resolution_answer = new ArrayList<>();
		
		for (Predicate first_inner : first)
		{
			for (Predicate second_inner : second)
			{
				if (!first_inner.name.equals(second_inner.name))
				{
					break;
				}
				else if (first_inner.is_negated && second_inner.is_negated
						|| !first_inner.is_negated && !second_inner.is_negated)
				{
					break;
				}
				else
				{
					HashMap<String, String> substitutions = unify(first_inner, second_inner);
					
					if (!substitutions.isEmpty())
					{
						ArrayList<Predicate> partial = new ArrayList<>();
						
						for (int j=0; j<first.size(); j++)
						{
							if (first.get(j)!=first_inner)
								partial.add(createDeepCopy(first.get(j)));
						}
						
						for (int l=0; l<second.size(); l++)
						{
							if (second.get(l)!=second_inner)
								partial.add(createDeepCopy(second.get(l)));
						}
						
						doSubstitution(partial, substitutions);
						resolution_answer.add(partial);
					}	
				}
			}
		}
		return resolution_answer;
	}
	
	
	public static HashMap<String, ArrayList<ArrayList<Predicate>>> cloneHashMap (HashMap<String, ArrayList<ArrayList<Predicate>>> originalHashMap)
	{
		HashMap<String, ArrayList<ArrayList<Predicate>>> copy_hm = new HashMap<>();
		for (String key : originalHashMap.keySet())
		{
			ArrayList<ArrayList<Predicate>> copy_hm_lol = new ArrayList<>();
			ArrayList<ArrayList<Predicate>> original_lol = originalHashMap.get(key);
			
			for (ArrayList<Predicate> original_loll : original_lol)
			{
				ArrayList<Predicate> copy_hm_loll = new ArrayList<>();
				for (Predicate original_predicate : original_loll)
				{
					copy_hm_loll.add(createDeepCopy(original_predicate));
				}
				copy_hm_lol.add(copy_hm_loll);
			}
			copy_hm.put(key, copy_hm_lol);
			
		}
		
		return copy_hm;
	}
	
	public static Predicate createDeepCopy(Predicate original)
	{
		Predicate copy = new Predicate();
		copy.name = original.name;
		copy.variables = new String[original.variables.length];
		
		for (int i=0; i<copy.variables.length; i++)
		{
			copy.variables[i] = original.variables[i];
		}
		
		copy.is_negated = original.is_negated;
		
		return copy;
	}
	
	
	public static boolean checkVariable(String check)
	{
		if (check.length()==1 && Character.isLowerCase(check.charAt(0)))
			return true;
		else
			return false;
	}
	
	public static boolean checkConstant(String check)
	{
		if (Character.isUpperCase(check.charAt(0)))
			return true;
		else
			return false;
	}
	
	public static HashMap<String, String> unify(Predicate one, Predicate two)
	{
		HashMap<String,String> var_to_var = new HashMap<>();
		HashMap<String, String> substitutions = new HashMap<>();
		for (int x=0; x<one.variables.length; x++)
		{
			boolean one_condition = false;
			if (checkConstant(one.variables[x]) && checkVariable(two.variables[x]))
			{
				if (!substitutions.containsKey(two.variables[x]))
				{
					substitutions.put(two.variables[x],one.variables[x]);
					one_condition = true;
				}
				else
				{
					if(substitutions.get(two.variables[x]).equals(one.variables[x]))
					{
						one_condition = true;
					}
				}
					
			}
			else if (checkConstant(two.variables[x]) && checkVariable(one.variables[x]))
			{
				if (!substitutions.containsKey(one.variables[x]))
				{
					substitutions.put(one.variables[x],two.variables[x]);
					one_condition = true;
				}
				else
				{
					if(substitutions.get(one.variables[x]).equals(two.variables[x]))
					{
						one_condition = true;
					}
				}
			}
			else if (checkVariable(one.variables[x]) && checkVariable(two.variables[x]))
			{
				if (one.variables[x].equals(two.variables[x]))
				{
					if(!var_to_var.containsKey(one.variables[x]))
					{
						var_to_var.put(one.variables[x], two.variables[x]);
					}
					one_condition = true;
				}
			}
			else if (checkConstant(one.variables[x]) && checkConstant(two.variables[x]))
			{
				if (one.variables[x].equals(two.variables[x]))
				{
					if(!substitutions.containsKey(one.variables[x]))
					{
						substitutions.put(one.variables[x], two.variables[x]);
					}
					one_condition = true;
					
				}
			}
			
			if (!one_condition)
			{
				var_to_var.clear();
				substitutions.clear();
				return substitutions;
			}
		}
		
		if(substitutions.isEmpty())
			return var_to_var;
		else
			return substitutions;
	}
	
	public static void doSubstitution(ArrayList<Predicate> argument, HashMap<String, String> substitutions)
	{
		for (int w=0; w<argument.size(); w++)
		{
			Predicate obj = argument.get(w);
			for (int l=0; l<obj.variables.length; l++)
			{
				if (substitutions.containsKey(obj.variables[l]))
				{
					obj.variables[l] = substitutions.get(obj.variables[l]);
				}
			}
		}
	}
	
	public static void main (String args[]) throws IOException
	{
		WrappedInput input_obj = readInput();
		HashMap<String, ArrayList<ArrayList<Predicate>>> hm = createKB(input_obj.ar);
		String answers[] = new String[input_obj.queries.length];
		for (int x=0; x<input_obj.queries.length; x++)
		{
			if (canQueryBeInferred(input_obj.queries[x],hm))
			{
				answers[x] = "TRUE";
			}
			else
			{
				answers[x] = "FALSE";
			}
		}
		
		writeOutputToFile(answers);
	}
	
	public static void writeOutputToFile(String answers[]) throws IOException
	{
		BufferedWriter bw = new BufferedWriter(new FileWriter("output.txt"));
		for (int i=0; i<answers.length; i++)
		{
			bw.write(answers[i]);
			bw.write("\n");
		}
		bw.close();
	}
	
	public static void printPredicateArray(Predicate arr[])
	{
		System.out.print("[");
		for (int i=0; i<arr.length; i++)
		{
			System.out.print(predicateStringRep(arr[i])+" ");
		}
		System.out.print("]");
		System.out.println();
	}
	
	public static ArrayList<String> checkDuplicatesWithin(ArrayList<String> arlist)
	{
		ArrayList<String> str_copy = new ArrayList<>();
		for (String obj : arlist)
		{
			if (!str_copy.contains(obj))
			{
				str_copy.add(obj);
			}
		}
		
		return str_copy;
	}

}

class Predicate
{
	String name;
	String variables [];
	boolean is_negated;
	
}

class WrappedInput
{
	Predicate queries[];
	ArrayList<ArrayList<Predicate>> ar;
}
